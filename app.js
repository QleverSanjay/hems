var createError = require("http-errors");
var express = require("express");
var expressValidator = require("express-validator");
var path = require("path");
var cookieParser = require("cookie-parser");
var bodyParser = require("body-parser");
var logger = require("morgan");
var session = require("express-session");
var indexRouter = require("./routes/index");
var usersRouter = require("./routes/users");
var mongoose = require("mongoose");
var ExpressValidator = require("express-validator");
var passport = require("passport");
var MongoStore = require("connect-mongo")(session);
var flash = require("connect-flash");
var createAdmin = require("./createAdmin");
var User = require("./models/user.js");

var app = express();

// view engine setup
app.set("views", path.join(__dirname, "views"));
app.set("view engine", "ejs");

//morgan logger to see api hits and fails
app.use(logger("dev"));

//parses the request bodies
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());

//to acces the static images styles and js
app.use(express.static(path.join(__dirname, "public")));



//Get the default connection
var db = mongoose.connection;
mongoose.connect("mongodb://QleverSanjay:QleverSanjay14@ds157383.mlab.com:57383/hems");

//Bind connection to error event (to get notification of connection errors)
db.on("error", console.error.bind(console, "MongoDB connection error:"));

createAdmin.seed();

app.use(
  session({
    secret: "HEMS Users",
    resave: false,
    saveUninitialized: false,
    rolling: true,
    store: new MongoStore({ mongooseConnection: mongoose.connection }),
    cookie: { maxAge: 60 * 60000 }
  })
);

//middlewares that wraps validator.js validator and sanitizer functions. like isEmail,isPhone
app.use(ExpressValidator());

// Configure Passport
app.use(passport.initialize());
app.use(passport.session());
// passport.use(User.createStrategy());

// passport.serializeUser(User.serializeUser());
// passport.deserializeUser(User.deserializeUser());

app.use(flash());
app.use((req, res, next)=>{
  res.locals.success_msg = req.flash('success_msg');
  res.locals.error_msg = req.flash('error_msg');
  res.locals.error = req.flash('error');
  res.locals.user = req.user || null;
  res.locals.session = req.session;
  next();
});

//routing folders integration
app.use("/", indexRouter);
app.use("/users", usersRouter);



// // catch 404 and forward to error handler
// app.use(function(req, res, next) {
//   next(createError(404));
// });

// // error handler
// app.use(function(err, req, res, next) {
//   // set locals, only providing error in development
//   res.locals.message = err.message;
//   res.locals.error = req.app.get("env") === "development" ? err : {};

//   // render the error page
//   res.status(err.status || 500);
//   res.render("error");
// });

module.exports = app;
