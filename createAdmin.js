var mongoose = require("mongoose");
var User = require("./models/user.js");

exports.seed = function() {
  //credentials for the super admins
  var user = {
    username: "admin",
    password: "admin",
    name: { first: "admin", last: "admin" },
    contact_number: "123456789",
    email: "admin@hems.com",
    role: 0,
    full_address: { address: "bangalore", zip_code: "560032" }
  };

  var admin = new User(user);
  User.find({
    $and: [{ email: admin.email }, { role: 0 }]
  }).then(doc => {
    if (doc.length == 0)
      admin.save().catch(err => {
        console.log(err);
      });
  });
};
