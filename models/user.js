var mongoose = require("mongoose");
var localStartegy = require("passport-local-mongoose");
const bcrypt = require("bcryptjs");
//automatically takes care of salting and hashing the password

var UserSchema = new mongoose.Schema({
  username: {
    type: String,
    unique: true,
    required: true,
    trim: true
  },
  password: {
    type: String,
    required: true
  },
  name: {
    first: { type: String, required: [true, "can't be blank"], trim: true },
    last: { type: String, trim: true }
  },
  contact_number: {
    type: String,
    required: [true, "can't be blank"],
    trim: true
  },
  email: {
    type: String,
    required: [true, "can't be blank"],
    trim: true,
    match: [/\S+@\S+\.\S+/, "Email is invalid"]
  },
  role: {
    type: Number,
    required: [true, "can't be blank"],
    default: 1
  }, // {0: 'admin', 1: user (customer in this case)}
  is_active: {
    type: String,
    enum: ["Y", "N"],
    required: [true, "can't be blank"],
    default: "N"
  }, // {0: 'inactive', 1: 'active'}
  full_address: {
    address: { type: String, required: [true, "can't be blank"], trim: true },
    city: { type: String, trim: true },
    country: { type: String, trim: true },
    zip_code: {
      type: Number,
      required: [true, "can't be blank"],
      trim: true
    }
  },
  forgetPassword: {
    token: { type: String, trim: true},
    sentTime: { type: Date }
  },
  is_configured: {
    type: String,
    enum: ["Y", "N"],
    default: "N"
  },
  device_id : [{
    type: mongoose.Schema.Types.ObjectId,
    ref: 'devices'
}]
});

UserSchema.pre("save", function(next) {
  var user = this;
  if (user.isModified("password")) {
    bcrypt.genSalt(5, (err, salt) => {
      bcrypt.hash(user.password, salt, (err, hash) => {
        user.password = hash;
        next();
      });
    });
  } else {
    next();
  }
});

UserSchema.plugin(localStartegy);
module.exports = mongoose.model("User", UserSchema);
