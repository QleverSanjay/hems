var mongoose = require("mongoose");
const Schema = mongoose.Schema;

var ReadingsSchema = Schema({
  Device_id: {
    type: String
  },
  time : {
      type : Date
  },
  readings : {
    type : Object
  }
});


module.exports = mongoose.model("Readings", ReadingsSchema);
