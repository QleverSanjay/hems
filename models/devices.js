var mongoose = require("mongoose");
var DeviceSchema = new mongoose.Schema({
    name : {
        type : String,
        required : true,
        trim : true
    },
    brand : {
        type : String,
        trim : true
    },
    model : {
        type : String,
        trim : true
    },
    usage : {
        type : String,
        trim : true
    },
    user_id : {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'users'
    }
});

module.exports = mongoose.model("Device", DeviceSchema);