const express = require('express');
const router = express.Router();
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const bcrypt = require('bcryptjs');

var User = require('./models/user.js');

var authenticate = passport.use(new LocalStrategy(
    (username, password, done)=>{
        User.findOne({username}).then((user)=>{
            console.log(user);
            if(!user){ 
                return done(null, false, {message: 'Incorrect username'});
            }if(user.is_active == 'N'){
                return done(null, false, {message: 'Account has not been Activated yet. Please Wait for it to Activate.!!'});
            }
            // if(![0,1,2].includes(user.role)){
            //     return done(null, false, {message: 'Unauthorized'});
            // }
            bcrypt.compare(password, user.password).then((valid)=>{
                if(valid){
                    return done(null, user);
                }else return Promise.reject();
            }).catch(()=>{
                 return done(null, false, {message: 'Incorrect password'});
            });          
        });
    }
));

passport.serializeUser((user, done)=>{
    console.log("user s");
    done(null, user.id);
});

passport.deserializeUser((id, done)=>{
    
    User.findById(id).then((user)=>{
        console.log("des",user);
        done(null, user);
        if(user.role == 4) return Promise.reject();
    }).catch((err)=>{
        console.log(err);
    });
});


var isSuperAdmin = (req, res, next) => {
    if(req.user){
        if(req.user.role == 0) {
            next();
        }
    }else {
        res.render('index', {error: "You must be authenticated.."});
    }
};

var isLogin = (req, res, next) => {
    if(req.user) {
        next();
    }else {
        res.render('index', {error: "You must be authenticated.."});
    }
}
module.exports = {authenticate, isLogin, isSuperAdmin};