const nodemailer = require('nodemailer');
const ejs = require('ejs');
const async  =   require('async');
const path = require('path');
const sendgridCredentials = require('./sendgridCredential.json');

var sendMail = (options, callback) => {
    /* options = {
    from: String,
    to: String,
    cc: String,
    bcc: String,
    text: String,
    textPath String,
    html: String,
    htmlPath: String,
    attachments: [String],
    success: Function,
    error: Function
  } */

    var renderText = function (callback) {
        ejs.renderFile(path.join(__dirname, '..', '..', 'views', 'mail-templates', options.textPath), function (err, text) {
            if (err) {
                return callback(err, null);
            }
            else {
                options.text = text;
                return callback(null, 'done');
            }
        });
    };

    var renderHtml = function (callback) {
        var pathNew = path.join(__dirname, '..', '..', 'views', 'mail-templates', options.htmlPath);
        console.log(pathNew);
        ejs.renderFile(pathNew, { locals: options.locals }, function (err, html) {
            if (err) {
                return callback(err, null);
            }
            else {
                options.html = html;
                return callback(null, 'done');
            }
        });
    };

    var renderers = [];
    if (options.textPath) {
        renderers.push(renderText);
    }

    if (options.htmlPath) {
        renderers.push(renderHtml);
    }

    async.parallel(
        renderers,
        function (err, results) {
            if (err) {
                console.log('Email template render failed. ' + err);
                return;
            }

            var attachments = [];

            if (options.html) {
                attachments.push({ data: options.html, alternative: true });
            }

            if (options.attachments) {
                for (var i = 0; i < options.attachments.length; i++) {
                    attachments.push(options.attachments[i]);
                }
            }

            // create reusable transporter object using the default SMTP transport




            //    console.log("here");
            console.log(sendgridCredentials);

            let transporter = nodemailer.createTransport(sendgridCredentials);

            // var fromEmail = `${fromName} <@${sendgridCredentials.domain}>`;
            let mailOptions = {
                from: options.from, // sender address
                to: options.to, // list of receivers
                subject: options.subject, // Subject line
                text: options.text,   // plain text body
                html: options.html, // html body,
                attachments:options.attachments
            };


            transporter.sendMail(mailOptions, (error, info) => {
                if (error) {
                    console.log(error);
                    callback(err, null);
                }else{

                }
                console.log(info);
                console.log('Message sent: %s', info.messageId);
            // Preview only available when sending through an Ethereal account
                console.log('Preview URL: %s', nodemailer.getTestMessageUrl(info));
                return callback(null, 'done');
            });
        }
    );
};

var sendOtp = (number, otp) => {
    request('GET', `https://enterprise.smsgupshup.com/GatewayAPI/rest?method=SendMessage&send_to=91${number}&msg=${otp}%20is%20your%20One%20Time%20Password%20for%202B!%20Radio%20Registration,%20Valid%20for%2030%20Mins.Thank%20you%20for%20using%202B!%20Radio.&msg_type=TEXT&userid=2000175592&auth_scheme=plain&password=ktqB83AX1&v=1.1&format=text`).done((res) => {
      console.log("this is the new sms api", res.getBody('utf8'));
  });

}

module.exports = { sendMail };