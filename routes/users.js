var express = require("express");
var router = express.Router();
var User = require("../models/user");
var Device = require("../models/devices");
var Reading = require("../models/readings");
var validator = require("validator");
var moment = require('moment');
var {
  authenticate,
  isLogin,
  isSuperAdmin
} = require("./../passport-handler.js");
var flash = require("connect-flash");
const { sendMail } = require("./../config/SendgridMail/sendgridMailService");
const cheerio = require("cheerio");

/* GET users listing. */
router.get("/", function(req, res, next) {
  res.send("respond with a resource");
});

router.post("/register", function(req, res, next) {
  var name = req.body.firstName;
  const email = req.body.email;
  const phone = req.body.contact_number;
  const username = req.body.username;
  if (!name) return res.send({ code: 1, message: "Please enter name" });
  if (!email) return res.send({ code: 1, message: "Please enter email" });
  if (!validator.isEmail(email))
    return res.send({ code: 1, message: "Email not is in valid format" });
  if (!phone)
    return res.send({ code: 1, message: "Please enter phone number" });
  if (!validator.isMobilePhone(phone, "any"))
    return res.send({ code: 1, message: "Mobile number not valid" });
  if (!username)
    return res.send({ code: 1, message: "Old username is required" });

  req.body.name = { first: req.body.firstName, last: req.body.lastName };
  req.body.full_address = {
    address: req.body.address,
    city: req.body.city,
    country: req.body.country,
    zip_code: req.body.zip_code
  };
  console.log("Reg user data ->", req.body);
  user = new User(req.body);
  user
    .save(req.body)
    .then(usr => {
      console.log(usr);
      var mailTo = usr.email;
      var mailFrom = "admin@HEMS.com";
      var txt = `Thank You For Registering!! \n Your Login Credential are \n UserName : ${
        usr.username
      } \n PassWord : ${
        req.body.password
      } \n Please wait for the confirmation of your activation.`;
      sendMail(
        {
          from: mailFrom,
          to: mailTo,
          subject: "Welcome to HEMS!!",
          text: txt
        },
        function(err, info) {
          if (err) {
            console.log("Something went wrong!!!");
            //   res.send({code:1 , message:"Failed sending email"});
          } else {
            //   res.send({code:0 , message:"Email Sent."});
          }
        }
      );
      res.send({ code: 0, message: "User Registered succesfully." });
    })
    .catch(err => {
      console.log("Error in Registering User!! -> ", err);
      if (err.code == 11000) {
        res.send({
          code: 1,
          message: "User Already Exists with these data!!!"
        });
      } else {
        res.send({
          code: 1,
          message: "Server problem, Please try again later.."
        });
      }
    });
});

router.post("/updateProfile", (req, res, next) => {
  var name = req.body.firstName;
  // const email = req.body.email;
  const phone = req.body.contact_number;
  const username = req.body.username;
  if (!name) return res.send({ code: 1, message: "Please enter name" });
  // if (!email) return res.send({ code: 1, message: "Please enter email" });
  // if (!validator.isEmail(email))
  //   return res.send({ code: 1, message: "Email not is in valid format" });
  if (!phone)
    return res.send({ code: 1, message: "Please enter phone number" });
  if (!validator.isMobilePhone(phone, "any"))
    return res.send({ code: 1, message: "Mobile number not valid" });
  if (!username)
    return res.send({ code: 1, message: "Old username is required" });

  req.body.name = { first: req.body.firstName, last: req.body.lastName };
  req.body.full_address = {
    address: req.body.address,
    city: req.body.city,
    country: req.body.country,
    zip_code: req.body.zip_code
  };
  console.log("Reg user data ->", req.body);
  // user = new User(req.body);
  User.findByIdAndUpdate(req.user._id, req.body)
    .then(usr => {
      console.log(usr);
      res.send({ code: 0, message: "User Profile Updated." });
    })
    .catch(err => {
      console.log("Error in Registering User!! -> ", err);
      if (err.code == 11000) {
        res.send({
          code: 1,
          message: "User Already Exists with these data!!!"
        });
      } else {
        res.send({
          code: 1,
          message: "Server problem, Please try again later.."
        });
      }
    });
});

router.post(
  "/login",
  authenticate.authenticate("local", {
    successRedirect: "/redirect",
    failureRedirect: "/",
    failureFlash: true
  }),
  (req, res) => {
    res.redirect("/");
  }
);
router.get("/home", isLogin, function(req, res, next) {
  console.log("user==", req.user);
  if (req.user.is_configured == "Y") {
    var devices = [];
    Device.find({ user_id: req.user._id })
      .then(devices => {
        res.render("home", { active: "dashboard", devices: devices });
      })
      .catch(err => {
        console.log(err);
        res.render("home", { active: "dashboard", devices: [] });
      });
  } else {
    res.render("user-configure");
  }
});
router.get("/logout", (req, res) => {
  req.logout();
  res.redirect("/");
});

router.post("/saveDevices",isLogin, (req, res, next) => {
  var arr = JSON.parse(req.body.data);
  console.log("arr====", arr);
  arr.forEach(function(a, i) {
    a.user_id = req.user._id;
    var device = new Device(a);
    device.save().then(dvc => {
      console.log(dvc);
      User.findByIdAndUpdate(
        req.user._id,
        { $push: { device_id: dvc._id }, $set: { is_configured: "Y" } },
        { new: true }
      )
        .then(user => {
          console.log("User updated ", user);
          console.log(i, arr.length);
          if (i == arr.length - 1) {
            req.user = user;
            res.send({
              code: 0,
              message: "Successfull"
            });
          }
        })
        .catch(err => {
          console.log(err);
          res.send({
            code: 1,
            message: "Server problem, Contact admin."
          });
        })
        .catch(err => {
          console.log(err);
          res.send({
            code: 1,
            message: "Server problem, Contact admin."
          });
        });
    });
  });
});

router.post("/updateDevice",isLogin, (req, res, next) => {
  var device = JSON.parse(req.body.data);
  console.log(device);
  Device.findByIdAndUpdate(device.id,device).then(dvc=>{
    res.send({
      code: 0,
      message: "Successfull"
    });
  }).catch(err=>{
    res.send({
      code: 1,
      message: "Server Problem!! Try Again Later.."
    });
  })
});

router.get("/showAnalytics/:uid", (req, res, next) => {
  Device.find({ user_id: req.params.uid })
  .then(devices => {
    if(devices.length >0){
    res.send({code: 0,devices: devices });
    }else{
      res.send({code: 1,message: "No Devices for this user!!" });
    }
  }).catch(err => {
    console.log(err);
    res.send({code: 1,message: "Server Problem!!" });
  });
});

router.get('/getChartReadings',isLogin,(req, res, next) => {
  Reading.aggregate([
    {$group: {
      _id: {$month: '$time'},
      maxVolts: {$max: '$readings.volts'},
       maxAmperes: {$max: '$readings.amperes'},
       maxWatts: {$max: '$readings.watts'}
      
      }}]).then(function(data){
              console.log(data)
              res.send({data: data});
            }).catch(err=>{
              console.log(err);
            })
});

module.exports = router;
