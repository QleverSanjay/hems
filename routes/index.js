var express = require("express");
var router = express.Router();
const otpGenerator = require("otp-generator");
var User = require("../models/user");
var Device = require("../models/devices");
var Readings = require("../models/readings");
const { sendMail } = require("./../config/SendgridMail/sendgridMailService");
var {
  authenticate,
  isLogin,
  isSuperAdmin
} = require("./../passport-handler.js");
var moment = require('moment');
var fs = require('file-system');
/* GET home page. */
router.get("/", function(req, res, next) {
  // var arr = [];
  // Readings.find({}).then(r=>{
  //   var str = "";
  //   r.forEach(e=>{
  //     // arr.push(  moment.utc(e.time).format('YYYY-MM-DD hh:mm:ss A'));
  //     // console.log (moment.utc(e.time).format('YYYY-MM-DD hh:mm:ss A'));
  //     console.log(e.Device_id?e.Device_id:" - ",e.readings.amperes?e.readings.amperes:" - ",e.readings.pf_factor?e.readings.pf_factor:" - ",e.readings.volts?e.readings.volts:" - ",e.readings.watts?e.readings.watts:" - ",e.time);
  //     arr.push(moment.utc(e.time).format('YYYY-MM-DD hh:mm:ss A')+"\n");

  //   });
  //   console.log(str);
  //   fs.writeFile("C:\\Users\\Qleverlabs\\Desktop\\PEM\\test", arr.toString(), function(err) {
  //       if(err) {
  //           return console.log(err);
  //       }
    
  //       console.log("The file was saved!");
  //   }); 
  //   console.log(arr.toString());
  // }).catch(err=>{
  //   console.log(err);
  // });
  res.render("index");
});
router.get("/signup", function(req, res, next) {
  res.render("user-signup");
});
router.post("/forgetpassword", function(req, res, next) {
  var email = req.body.email;
  const resetToken = otpGenerator.generate(23, {
    digits: true,
    alphabets: true,
    specialChars: false,
    upperCase: false
  });
  var link = process.env.NODE_ENV
    ? "http://hems.qleverlabs.in/resetpassword/" + resetToken
    : "http://localhost:4201/resetpassword/" + resetToken;
  var sentTime = new Date();
  User.findOne({ email: email })
    .then(user => {
      if (user) {
        user.forgetPassword = { token: resetToken, sentTime: sentTime };
        user
          .save()
          .then(usr => {
            console.log("User Password reset link sent -", email);
            var txt = `Trouble signing in? \n Resetting your password is easy. Just press the link below and follow the instructions. We'll have you up and running in no time. \n${link}`;
            sendMail(
              {
                from: "no-reply@HEMS.com",
                to: email,
                subject: "Password Reset.",
                text: txt
              },
              function(err, info) {
                if (err) {
                  console.log("Something went wrong!!!");
                  //   res.send({code:1 , message:"Failed sending email"});
                } else {
                  //   res.send({code:0 , message:"Email Sent."});
                }
              }
            );
            res.send({ code: 0, message: "Password Link Sent." });
          })
          .catch(err => {
            res.send({
              code: 1,
              message: "Server Problem!! Please contact Admin"
            });
            console.log("Error in changing password!!", err);
          });
      } else {
        res.send({
          code: 1,
          message: "No user registered with this Email..!!"
        });
      }
    })
    .catch(err => {
      res.send({ code: 1, message: "Server Problem!! Please contact Admin" });
      console.log(err);
    });
});
router.get("/resetpassword/:token", function(req, res, next) {
  var token = req.params.token;
  var currentDate = new Date();
  console.log(currentDate);
  currentDate.setMinutes(currentDate.getMinutes() - 1200);
  console.log(currentDate);
  User.find({
    is_active: "Y",
    "forgetPassword.token": token,
    "forgetPassword.sentTime": { $gte: currentDate.toISOString() }
  })
    .count()
    .then(count => {
      console.log(count);
      if (count == 1) {
        res.render("PasswordResetPage");
      } else {
        res.render("ErrorLink", { message: "Link is Expired or broken" });
      }
    })
    .catch(err => {
      res.send({
        code: 1,
        message: "Server Problem!! Please contact Admin"
      });
      console.log("Error in changing password!!", err);
    });
});
router.post("/resetpassword/:token", function(req, res, next) {
  var token = req.params.token;
  var currentDate = new Date();
  console.log(currentDate);
  currentDate.setMinutes(currentDate.getMinutes() - 1200);
  console.log(currentDate);
  User.find({
    is_active: "Y",
    "forgetPassword.token": token,
    "forgetPassword.sentTime": { $gte: currentDate.toISOString() }
  })
    .then(users => {
      var count = users.length;
      console.log(count);
      if (count == 1) {
        var user = users[0];
        user.password = req.body.password;
        user
          .save()
          .then(usr => {
            res.send({
              code: 0,
              message: "Password Changed Successfully"
            });
          })
          .catch(err => {
            res.send({
              code: 1,
              message: "Server Problem!! Please contact Admin"
            });
            console.log("Error in changing password!!", err);
          });
      } else {
        res.render("ErrorLink", { message: "Link is Expired or broken" });
      }
    })
    .catch(err => {
      res.send({
        code: 1,
        message: "Server Problem!! Please contact Admin"
      });
      console.log("Error in changing password!!", err);
    });
});

router.get("/redirect", function(req, res, next) {
  console.log(req.user);
  if (req.user.role == 0) {
    res.redirect("/adminDashboard");
  } else if (req.user) {
    res.redirect("/users/home");
  }
});
router.get("/adminDashboard", isSuperAdmin, function(req, res, next) {
  User.find({ role: { $ne: 0 } })
    .then(users => {
      res.render("adminDashboard", { active: "dashboard", allUsers: users });
    })
    .catch(err => {
      console.log(err);
    });
});
router.get("/changeStatus", isSuperAdmin, function(req, res, next) {
  console.log(req.query.uid, req.query.status);
  User.findByIdAndUpdate(req.query.uid, { is_active: req.query.status })
    .then(user => {
      // sendMail(
      //   {
      //     from: mailFrom,
      //     to: mailTo,
      //     subject: "Welcome to HEMS!!",
      //     text: txt
      //   },
      //   function(err, info) {
      //     if (err) {
      //       console.log("Something went wrong!!!");
      //       //   res.send({code:1 , message:"Failed sending email"});
      //     } else {
      //       //   res.send({code:0 , message:"Email Sent."});
      //     }
      //   }
      // );
      res.send({
        code: 0,
        message: "User Updated"
      });
    })
    .catch(err => {
      console.log(err);
    });
});

const io = require('socket.io').listen('4202');


router.post("/saveReadings", function(req, res, next) {
  console.log(req.body);
  var data = req.body;
  var deviceIds = Object.keys(data);
  console.log(deviceIds);
  var faultyIds = [];
  
  deviceIds.forEach(id => {
    console.log(id);
    var rgs = { Device_id: id, time: new Date(), readings: data[id] };
    io.emit('get-reading', rgs);
    var readings = new Readings(rgs);
    readings
      .save()
      .then(reading=>{
        console.log(id, "succesfully saved....");
      })
      .catch(err=>{
        faultyIds.push(id);
        console.log(err);
      });
  });

  if(faultyIds.length > 0){
    res.send({code: 1,message : "Following Data didnt get saved!!", data : faultyIds});
  }else{
    res.send({code: 0,message : "Sucessfully sent"});
  }
});

router.get("/showUserDashboard/:uid", (req, res, next) => {

  Device.find({ user_id: req.params.uid })
  .then(devices => {
    console.log(devices);
    User.findById(req.params.uid).then(user=>{
      console.log(user);
      res.render("home", { active: "admin-dashboard",devices: devices, user :  user});
    }) .catch(err => {
      console.log(err);
      res.render("home", { active: "admin-dashboard",devices: devices , user : {} });
    });
  })
  .catch(err => {
    console.log(err);
    res.render("home", { active: "admin-dashboard",devices: [] });
  });

});

router.get('/getDashabordChart',(req,res,next)=>{

  var dte = new Date();
  dte.setHours( dte.getHours() - 12 );
  console.log(dte,moment(new Date(req.query.date)).subtract(24, "hours").format());
  var query = {
      "$gt": dte.toISOString()
  }
  if(req.query.date){
    console.log(req.query.date);
    var newDate = req.query.date;
    var dt = new Date(newDate);
    dt.setHours( dt.getHours() + 24 );
    query = {
          "$gt": new Date(newDate),
          "$lte" : moment(new Date(newDate)).add(24, "hours").format()
    }
  }

  Readings.find({

    "Device_id" : "deviceId1",
 
          "time" :  query
 
    }).then(r=>{
     console.log(r.length);
      let op = JSON.stringify(r);
      let p = JSON.parse(op);

      // p.forEach(o => o.hr = moment(o.time).format('HH'));
      
const labels = [];

const map = new Map();
  const volts = [];
  const amperes = [];
  const watts = [];

for (const e of p) {
  e.hr = moment(e.time).format('HH');
   if(!map.has(e.hr)){
       map.set(e.hr, true);    // set any value to Map
       var v = 0;
       var a = 0;
       var vw = 1;
       var aw = 1;
       var pf = 0.8;
       if(e.readings.volts && e.readings.volts != 0){
        v = e.readings.volts;
        vw = e.readings.volts;
       }
       if(e.readings.amperes && e.readings.amperes != 0){
        a = e.readings.amperes;
        aw = e.readings.amperes;
       }
       if(e.readings.pf_factor && e.readings.pf_factor != 0){
        pf = e.readings.pf_factor;
       }
       var w =  pf*a*v;
      //  console.log(v,a,pf,w);
       labels.push(`${(moment(e.time).format('DD/MM/YY HH'))}hr`);
       volts.push(v);
      amperes.push(a);
      watts.push(w.toFixed(2));
   }
}

res.send({code : 1 ,labels,volts,amperes,watts})
    //   console.log(r.length,":::");
    //   const hours = new Set();
    //   const volts = [];
    //   const amperes = [];
    //   const watts = [];
    //   var hr = '';

    //   const result = [];

    //   const map = new Map();
    //   for (const e of r) {
    //     if(!map.has(e.time)){
    //         map.set(moment(e.time).format('hh a'), true);    // set any value to Map
    //         result.push({
    //             hour: moment(e.time).format('hh a'),
    //             volts: (e.readings.volts)?(e.readings.volts):0,
    //            amperes : (e.readings.amperes)?(e.readings.amperes):0,
    //            watts : (e.readings.pf_factor)?(e.readings.pf_factor):1*(e.readings.volts)?(e.readings.volts):1*(e.readings.amperes)?(e.readings.amperes):1
    //         });
    //     }
    //  }
    //  res.send({code : 1 ,result})

      // r.forEach(e=>{
      //   console.log(e);
      //   hours.add(moment(e.time).format('hh a'));
      //   if(moment(e.time).format('MM') == 02 && hr != moment(e.time).format('HH:MM')){
      //     console.log(":::here");
      //     volts.push((e.readings.volts)?(e.readings.volts):0);
      //     amperes.push((e.readings.amperes)?(e.readings.amperes):0);
      //     watts.push((e.readings.pf_factor)?(e.readings.pf_factor):1*(e.readings.volts)?(e.readings.volts):1*(e.readings.amperes)?(e.readings.amperes):1);
      //     hr = moment(e.time).format('HH:MM');
      //   }else if(moment(e.time).format('MM') == 00  && hr != moment(e.time).format('HH:MM')){
      //     console.log(":::here::2");
      //     volts.push((e.readings.volts)?(e.readings.volts):0);
      //     amperes.push((e.readings.amperes)?(e.readings.amperes):0);
      //     watts.push((e.readings.pf_factor)?(e.readings.pf_factor):1*(e.readings.volts)?(e.readings.volts):1*(e.readings.amperes)?(e.readings.amperes):1);
      //     hr = moment(e.time).format('HH:MM');
      //   }
      //   // console.log(moment(e.time).format('MMMM Do YYYY, h:mm:ss a'));
      //   // console.log(day,month,year,time);
      // });

      // volts.forEach(q=>{
      //   console.log(q);
      // })
      // let labels = Array.from(hours);
      // res.send({code : 1 ,labels,volts,amperes,watts})
    }).catch(err=>{
      console.log(err);
    });
});

module.exports = router;
